import React, {Component} from 'react';
import './App.css';
import Circle from "./components/Circle";

class App extends Component{
  state = {
    numbers: []
  };

  changeNumbers = () => {
    let sortNum = [];
    for (let i = 0; i < 5; i++) {
      let val = (Math.floor(Math.random() * (32) ) + 5);
      if (sortNum.indexOf(val) < 0) {
        sortNum.push(val);
      } else i--
    }
    sortNum.sort(function(a, b) {
      return a - b;
    });
    this.setState({numbers: sortNum});
  };

  render() {
    const circles = this.state.numbers.map(value => {
      return <Circle value={value} key={this.state.numbers.indexOf(value)} />
    })
    return (
        <div className="App">
          <div className="circles-block">
          {circles}
          </div>
          <button onClick={this.changeNumbers}>
            New numbers
          </button>
        </div>

    )
  }
}


export default App;
