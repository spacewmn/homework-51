import React from "react";

const Circle = props => {
    return (
        <div className='circle'>
            {props.value}
        </div>
    )
};

export default Circle;